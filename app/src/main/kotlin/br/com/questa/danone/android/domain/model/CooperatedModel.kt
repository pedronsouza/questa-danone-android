package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class CooperatedModel(
        @Expose var id : String?,
        @Expose @SerializedName("fantasy_name") var fantasyName : String?,
        @Expose @SerializedName("corporate_name") var corporateName : String?,
        @Expose var cnpj : String?,
        @Expose @SerializedName("flag_name") var flagName : String?) : Parcelable {

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<CooperatedModel> = object : Parcelable.Creator<CooperatedModel> {
            override fun createFromParcel(source: Parcel): CooperatedModel = CooperatedModel(source)
            override fun newArray(size: Int): Array<CooperatedModel?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(),
                                        source.readString(),
                                        source.readString(),
                                        source.readString(),
                                        source.readString())

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(fantasyName)
        dest?.writeString(corporateName)
        dest?.writeString(cnpj)
        dest?.writeString(flagName)
    }

    override fun describeContents(): Int = 0
}