package br.com.questa.danone.android.support

import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class DateUtils {
    companion object {
        val defaultZoneId = ZoneId.ofOffset("UTC", ZoneOffset.UTC)
        val formatter by lazy { DateTimeFormatter.ofPattern("yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'", Locale.US) }
        fun localDateTimeFrom(source: String) : LocalDateTime {
            return LocalDateTime.parse(source, formatter)
        }

        fun stringFrom(source: LocalDateTime) : String {
            return source.format(formatter)
        }

        fun now() : LocalDateTime {
            return LocalDateTime.now(defaultZoneId)
        }
    }
}