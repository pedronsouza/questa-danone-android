package br.com.questa.danone.android.domain.interactors

import br.com.questa.danone.android.domain.data.repositories.UserRepository
import br.com.questa.danone.android.domain.interactors.executors.PostExecutionThread
import br.com.questa.danone.android.domain.interactors.executors.ThreadExecutor
import br.com.questa.danone.android.domain.model.UserModel
import br.com.questa.danone.android.domain.net.bodies.SignInRequestBody
import br.com.questa.danone.android.support.Assertions
import rx.Observable
import javax.inject.Inject


class AuthenticateUserInteractor : BaseInteractor<UserModel> {
    lateinit var userRepository: UserRepository
    var credentials  : SignInRequestBody? = null

    @Inject constructor(userRepository: UserRepository, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : super(threadExecutor, postExecutionThread) {
        this.userRepository = userRepository
    }

    override fun observableFor(): Observable<UserModel> {
        Assertions.notNull(credentials, "Provide usermodel instance with the credentials to be validade")
        return this.userRepository.signInUserWithCredentials(credentials!!)
    }
}