package br.com.questa.danone.android.domain.data.datasources

import br.com.questa.danone.android.domain.model.UserModel
import br.com.questa.danone.android.domain.net.bodies.SignInRequestBody
import rx.Observable


interface AuthenticationDataSource {
    fun signUpUserWithCredentials(credentials: SignInRequestBody) : Observable<UserModel>
}