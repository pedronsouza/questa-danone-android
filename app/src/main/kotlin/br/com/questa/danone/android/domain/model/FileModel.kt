package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class FileModel(
        @Expose var image : String?,
        @Expose @SerializedName("image_key") var index : Int = -1,
        @Expose @SerializedName("image_url") var imageUrl : String?) : Parcelable {

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<FileModel> = object : Parcelable.Creator<FileModel> {
            override fun createFromParcel(source: Parcel): FileModel = FileModel(source)
            override fun newArray(size: Int): Array<FileModel?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readInt(), source.readString())

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(image)
        dest?.writeInt(index)
        dest?.writeString(imageUrl)
    }

    override fun describeContents(): Int = 0
}