package br.com.questa.danone.android.domain.screens


interface SignInScreen {
    fun redirectToSearchBookScreen()
    fun displayErrorTooltip()
    fun displayHelpTooltip()
}