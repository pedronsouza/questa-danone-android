package br.com.questa.danone.android.ui.widgets
import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet

open class EndlessRecyclerView : RecyclerView {
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { layoutManager = CustomLinearLayoutManager(context) }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) { layoutManager = CustomLinearLayoutManager(context) }
    constructor(context: Context) : super(context) { layoutManager = CustomLinearLayoutManager(context) }

    var listener : Listener? = null
    var rootCoordinator : CoordinatorLayout? = null
    var appbar : AppBarLayout? = null

    init {
        addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lManager = layoutManager as LinearLayoutManager
                val totalItemCount = lManager.itemCount
                val lastVisibleItem = lManager.findLastVisibleItemPosition()
                val visibleThreshold = 5

                if (rootCoordinator != null && dx != 0 && dy != 0) {
                    val params = appbar?.layoutParams as CoordinatorLayout.LayoutParams
                    val behavior = params.behavior
                    behavior?.onNestedFling(rootCoordinator, appbar, null, dx.toFloat(), dy.toFloat(), true)
                }


                if (totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    listener?.loadMore()
                }
            }
        })
    }

    class CustomLinearLayoutManager : LinearLayoutManager {
        constructor(context: Context) : super(context)
        constructor(context: Context, orientation: Int, reverseLayout: Boolean) : super(context, orientation, reverseLayout)

        override fun supportsPredictiveItemAnimations(): Boolean {
            return false
        }
    }

    interface Listener {
        fun loadMore()
    }
}