package br.com.questa.danone.android.domain.di.modules

import android.app.Application
import br.com.questa.danone.android.domain.data.datasources.AuthenticationDataSource
import br.com.questa.danone.android.domain.data.datasources.SearchBookDataSource
import br.com.questa.danone.android.domain.data.datasources.impl.RemoteAuthenticationDataSource
import br.com.questa.danone.android.domain.data.datasources.impl.RemoteSearchBookDataSource
import br.com.questa.danone.android.domain.data.repositories.BookRepository
import br.com.questa.danone.android.domain.data.repositories.UserRepository
import br.com.questa.danone.android.domain.net.HttpServices
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule(val application: Application) {
    @Singleton @Provides fun provideSearchBookDataSource(httpServices: HttpServices)            : SearchBookDataSource      = RemoteSearchBookDataSource(httpServices)
    @Singleton @Provides fun provideAuthenticationDataSource(httpServices: HttpServices)        : AuthenticationDataSource  = RemoteAuthenticationDataSource(httpServices)
    @Provides fun providesBookRepository(searchBookDataSource: SearchBookDataSource)            : BookRepository            = BookRepository(searchBookDataSource)
    @Provides fun providesUserRepository(authenticationDataSource: AuthenticationDataSource)    : UserRepository            = UserRepository(authenticationDataSource)
}