package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import br.com.questa.danone.android.DanoneApplication
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Suppress("UNCHECKED_CAST")
data class SearchBookModel(
        @Expose var data : Array<BookModel> = emptyArray<BookModel>(),
        @Expose var notice : String?,
        @Expose @SerializedName("photo_limit_notice") var photoNotice : String?,
        @Expose @SerializedName("mandatory_photos") var photos : MutableList<String> = mutableListOf(),
        @Expose @SerializedName("max_photos") var maxItems: Int = DanoneApplication.imageUploadLimit,
        @Expose var periods : List<PeriodModel>?,
        @Expose @SerializedName("current_page") var page : Int = 1,
        @Expose @SerializedName("hasMorePages") var hasMore : Boolean = false) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SearchBookModel> = object : Parcelable.Creator<SearchBookModel> {
            override fun createFromParcel(source: Parcel): SearchBookModel = SearchBookModel(source)
            override fun newArray(size: Int): Array<SearchBookModel?> = arrayOfNulls(size)
        }

        val SearchBookStatusApproved = 1
        val SearchBookStatusApprovedDouble = 3
        val SearchBookStatusReject = 2
        val SearchBookStatusOnServer = 4
    }

    constructor(source: Parcel) : this(source.readParcelableArray(BookModel::class.java.classLoader) as Array<BookModel>,
                                        source.readString(),
                                        source.readString(),
                                        mutableListOf(),
                                        source.readInt(),
                                        emptyList<PeriodModel>(),
                                        source.readInt(),
                                        (source.readInt() == 1)) {
        source.readList(photos, String::class.java.classLoader)
        source.readList(periods, PeriodModel::class.java.classLoader)
    }
    override fun describeContents(): Int  = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeParcelableArray(data, flags)
        dest?.writeString(notice)
        dest?.writeString(photoNotice)
        dest?.writeInt(maxItems)
        dest?.writeInt(page)
        dest?.writeInt(if (hasMore) 1 else 0)
        dest?.writeList(photos)
        dest?.writeList(periods)
    }
}