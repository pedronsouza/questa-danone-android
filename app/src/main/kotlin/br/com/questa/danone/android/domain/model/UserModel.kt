package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserModel(
        @Expose @SerializedName("user_id")      var id          : String? = null,
        @Expose @SerializedName("user_name")    var name        : String? = null,
        @Expose                                 var email       : String? = null,
        @Expose                                 var password    : String? = null,
        @Expose @SerializedName("group_id")     var groupId     : Int     = 0,
        @Expose @SerializedName("access_token") var accessToken : String? = null) : Parcelable {

    constructor(email: String?, password: String?) : this(null, null, email, password, 0, null)
    constructor(source: Parcel) :  this(source.readString(),
                                        source.readString(),
                                        source.readString(),
                                        source.readString(),
                                        source.readInt(),
                                        source.readString())

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<UserModel> = object : Parcelable.Creator<UserModel> {
            override fun createFromParcel(source: Parcel): UserModel = UserModel(source)
            override fun newArray(size: Int): Array<UserModel?> = arrayOfNulls(size)
        }
    }

    override fun describeContents(): Int = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(name)
        dest?.writeString(email)
        dest?.writeString(password)
        dest?.writeInt(groupId)
        dest?.writeString(accessToken)
    }



}