package br.com.questa.danone.android.domain.data.repositories

import br.com.questa.danone.android.domain.data.datasources.SearchBookDataSource
import br.com.questa.danone.android.domain.model.SearchBookModel
import rx.Observable
import javax.inject.Inject


class BookRepository {
    lateinit var searchBookDataSource: SearchBookDataSource

    @Inject constructor(searchBookDataSource: SearchBookDataSource) {
        this.searchBookDataSource = searchBookDataSource
    }

    fun searchForBookRegistry(query: String) : Observable<SearchBookModel> = this.searchForBookRegistry(query)
}