package br.com.questa.danone.android.ui.presenters

import br.com.questa.danone.android.domain.interactors.AuthenticateUserInteractor
import br.com.questa.danone.android.domain.interactors.subscribers.RunnableSubscriber
import br.com.questa.danone.android.domain.model.AppSession
import br.com.questa.danone.android.domain.model.UserModel
import br.com.questa.danone.android.domain.net.bodies.SignInRequestBody
import br.com.questa.danone.android.domain.screens.SignInScreen
import com.crashlytics.android.Crashlytics
import javax.inject.Inject


class SignInPresenter : BasePresenter {
    override fun unsubscribe() {
        this.authenticateUserInteractor.unsubscribe()
    }

    lateinit var authenticateUserInteractor: AuthenticateUserInteractor

    @Inject constructor(authenticateUserInteractor: AuthenticateUserInteractor) : super() {
        this.authenticateUserInteractor = authenticateUserInteractor
    }

    fun signInUser(email: String? = null, password: String? = null, screen: SignInScreen) {
        this.authenticateUserInteractor.credentials = SignInRequestBody(email, password)
        this.authenticateUserInteractor.execute(SignInUserSubscriber(screen, { sessionUser ->
            try {
                AppSession.create(sessionUser)
                screen.redirectToSearchBookScreen()
            } catch (e : Throwable) {
                Crashlytics.logException(e)
            }
        }))
    }

    class SignInUserSubscriber(val screen: SignInScreen, func : (UserModel) -> Unit) : RunnableSubscriber<UserModel>(func) {
        override fun onError(e: Throwable) {
            super.onError(e)
            Crashlytics.logException(e)
            screen.displayErrorTooltip()
        }
    }
}