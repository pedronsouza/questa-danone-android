package br.com.questa.danone.android.ui.views.activities

import android.os.Bundle
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.model.AppSession
import br.com.questa.danone.android.domain.screens.SendImagesButtonScreen
import br.com.questa.danone.android.ui.views.activities.bases.BaseActivity
import com.jakewharton.rxbinding.view.RxView
import rx.Subscription


class MainActivity : BaseActivity(), SendImagesButtonScreen {
    override fun layoutResourceId(): Int = R.layout.activity_main
    override fun redirectToLogin() = appRouter.toSignInScreen(this)
    override fun redirectToBookSearchScreen() = appRouter.toSearchBookScreen(this)

    var sendImagesBtSubs : Subscription? = null

    override fun onViewsBind(savedInstanceState: Bundle?) {

    }

    override fun unsubscribe() {
        super.unsubscribe()
        sendImagesBtSubs?.unsubscribe()
    }

    override fun onBindEvents() {
        super.onBindEvents()
        sendImagesBtSubs = RxView.clicks(findViewById(R.id.send_imagens_button)).subscribe {
            if (AppSession.currentSession() != null) redirectToBookSearchScreen() else redirectToLogin()
            unsubscribe()
        }
    }
}