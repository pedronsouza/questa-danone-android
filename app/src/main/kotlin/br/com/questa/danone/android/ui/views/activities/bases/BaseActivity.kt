package br.com.questa.danone.android.ui.views.activities.bases

import android.content.Context
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.model.AppSession
import br.com.questa.danone.android.ui.views.AppRouter
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity() {
    @Inject lateinit var appSession : AppSession
    @Inject lateinit var appRouter  : AppRouter

    protected var requestState : BackgroundExecutionState = BackgroundExecutionState.IDLE

    abstract fun onViewsBind(savedInstanceState: Bundle?)
    abstract fun layoutResourceId() : Int

    open protected fun inject() = DanoneApplication.appComponent?.inject(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResourceId())
        inject()
        onViewsBind(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        onBindEvents()
    }

    override fun onStop() {
        unsubscribe()
        super.onStop()
    }
    open fun onBindEvents() {}
    open fun unsubscribe() {}

    enum class BackgroundExecutionState {
        LOADING,
        IDLE
    }

    fun boldSpannable(context: Context, message: Int, span: Int, color: Int = 0) : Spannable {
        val string = context.getString(message)
        val stringToBold = context.getString(span)

        val out = SpannableString(string)
        val startIndex = string.indexOf(stringToBold)
        val endIndex = startIndex + stringToBold.length

        out.setSpan(StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        if (color != 0)
            out.setSpan(ForegroundColorSpan(ContextCompat.getColor(context, color)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)


        return out
    }

    fun onlyIfIdle(screenState: BackgroundExecutionState, func: () -> Unit) {
        if (screenState == BackgroundExecutionState.IDLE) {
            func()
        }
    }

    protected fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo!!.isConnected
    }

        protected fun showNoNetworkAlert() {
        AlertDialog.Builder(this)
        .setTitle("Alerta")
        .setMessage(R.string.no_network)
        .setPositiveButton("OK, Entendi", {view, which -> })
        .show()
    }
}