package br.com.questa.danone.android.domain.interactors

import br.com.questa.danone.android.domain.data.datasources.SearchBookDataSource
import br.com.questa.danone.android.domain.interactors.executors.PostExecutionThread
import br.com.questa.danone.android.domain.interactors.executors.ThreadExecutor
import br.com.questa.danone.android.domain.model.SearchBookModel
import rx.Observable
import javax.inject.Inject


class SearchBookInteractor : BaseInteractor<SearchBookModel> {
    lateinit var searchBookDataSource: SearchBookDataSource
    var query : String? = null
    var period : String? = null
    var page : Int = 1

    override fun observableFor(): Observable<SearchBookModel> = searchBookDataSource.searchBooksByQuery(query, page, period)

    @Inject
    constructor(searchBookDataSource: SearchBookDataSource, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : super(threadExecutor, postExecutionThread) {
        this.searchBookDataSource = searchBookDataSource
    }
}