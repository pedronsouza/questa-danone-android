package br.com.questa.danone.android.support;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;


import retrofit2.Converter;
import retrofit2.Retrofit;

public class EnumRetrofitConverterFactory extends Converter.Factory {
    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Converter<?, String> converter = null;
        if (type instanceof Class && ((Class<?>)type).isEnum()) {
            converter = new Converter<Object, String>() {
                @Override
                public String convert(Object value) throws IOException {
                    return EnumUtils.GetSerializedNameValue((Enum) value);
                }
            };
        }

        return converter;
    }
}