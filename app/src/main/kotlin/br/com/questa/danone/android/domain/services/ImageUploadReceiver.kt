package br.com.questa.danone.android.domain.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import br.com.questa.danone.android.domain.model.UploadItemModel

class ImageUploadReceiver(val delegate: Delegate) : BroadcastReceiver() {
    override fun onReceive(ctx: Context?, intent: Intent?) {
        val status = intent?.extras?.getInt(ImageUploadService.EXTENDED_DATA_STATUS)
        val uploadItem = intent?.extras?.getParcelable<UploadItemModel>(ImageUploadService.UPLOAD_ITEM_KEY)!!

        if (status == 200) {
            delegate.onSuccess(uploadItem)
        } else {
            delegate.onError(uploadItem)
        }
    }

    interface Delegate {
        fun onSuccess(uploadItemModel: UploadItemModel)
        fun onError(uploadItemModel: UploadItemModel)
    }
}