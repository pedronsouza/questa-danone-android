package br.com.questa.danone.android.domain.interactors.executors.impl

import br.com.questa.danone.android.domain.interactors.executors.PostExecutionThread
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject


class MainThreadExecutor : PostExecutionThread {
    @Inject
    constructor()

    override fun scheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}