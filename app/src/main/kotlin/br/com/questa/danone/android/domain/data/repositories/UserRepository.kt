package br.com.questa.danone.android.domain.data.repositories

import br.com.questa.danone.android.domain.data.datasources.AuthenticationDataSource
import br.com.questa.danone.android.domain.model.UserModel
import br.com.questa.danone.android.domain.net.bodies.SignInRequestBody
import javax.inject.Inject


class UserRepository {
    lateinit var authenticationDataSource: AuthenticationDataSource

    @Inject
    constructor(authenticationDataSource: AuthenticationDataSource) {
        this.authenticationDataSource = authenticationDataSource
    }

    fun signInUserWithCredentials(credentials: SignInRequestBody) = this.authenticationDataSource.signUpUserWithCredentials(credentials)
}