package br.com.questa.danone.android.domain.services

import android.app.IntentService
import android.content.Context
import android.content.Intent
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.domain.enums.UploadState
import br.com.questa.danone.android.domain.interactors.subscribers.RunnableSubscriber
import br.com.questa.danone.android.domain.model.FileModel
import br.com.questa.danone.android.domain.model.UploadItemModel
import br.com.questa.danone.android.domain.net.bodies.UploadResponseBody
import br.com.questa.danone.android.domain.services.events.OnImageUploadError
import br.com.questa.danone.android.domain.services.events.OnImageUploadSuccess
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import rx.Observable
import java.util.*


class ImageUploadService : IntentService(UPLOAD_SERVICE_KEY) {
    companion object {
        val UPLOAD_SERVICE_KEY : String = "UPLOAD_SERVICE_KEY"
        val UPLOAD_ITEM_KEY : String = "UPLOAD_ITEM_KEY"
        val BROADCAST_ACTION = "br.com.questa.danone.BROADCAST"
        val EXTENDED_DATA_STATUS = "br.com.questa.danone.STATUS"
    }

    override fun onHandleIntent(intent: Intent?) {
        val httpServices = DanoneApplication.appComponent?.httpServices()!!
        val mediaType = MediaType.parse("multipart/form-data; charset=utf-8;")
        val uploadItem = intent?.extras?.getParcelable<UploadItemModel>(UPLOAD_ITEM_KEY)!!
        val partImageName = "image[${uploadItem.index}]"
        val filename = "${uploadItem.index}_${Date().time}.jpg"
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), uploadItem.resource)
        val imagePart = MultipartBody.Part.createFormData(partImageName, filename, requestFile)
        val cooperatedIdRequestBody = RequestBody.create(mediaType, uploadItem.relatedBookModel.id)

        changeState(UploadState.PROGRESS, uploadItem)

        httpServices.cooperated.uploadImage(imagePart, cooperatedIdRequestBody)
                .flatMap { Observable.just(it.data) }
                .subscribe(ImageUploadSubscriber(this, uploadItem, { file ->
                    uploadItem.resourceUrl = file.url
            val allOk = file.stored && changeState(UploadState.SUCCESS,uploadItem)
            val fileModel = FileModel(file.url!!, uploadItem.index, file.url!!)
            if (allOk) {
                EventBus.getDefault().post(OnImageUploadSuccess(uploadItem, fileModel))
            } else {
                changeState(UploadState.FAILED,uploadItem)
                EventBus.getDefault().post(OnImageUploadError(uploadItem, fileModel))
            }
        }))
    }

    fun changeState(state: UploadState, uploadItem: UploadItemModel) : Boolean {
        uploadItem.state = state
        return DanoneApplication.appComponent?.uploadFileManager()?.store(uploadItem)!!
    }

    class ImageUploadSubscriber(val context: Context, val uploadItem : UploadItemModel, func: (UploadResponseBody) -> Unit) : RunnableSubscriber<UploadResponseBody>(func) {
        override fun onCompleted() {
            super.onCompleted()
        }

        override fun onError(e: Throwable) {
            if (changeState(UploadState.FAILED, uploadItem)) {
                EventBus.getDefault().post(OnImageUploadError(uploadItem))
            }

            super.onError(e)
        }

        fun changeState(state: UploadState, uploadItem: UploadItemModel) : Boolean {
            uploadItem.state = state
            return DanoneApplication.appComponent?.uploadFileManager()?.store(uploadItem)!!
        }
    }
}