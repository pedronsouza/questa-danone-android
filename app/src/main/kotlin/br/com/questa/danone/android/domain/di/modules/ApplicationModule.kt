package br.com.questa.danone.android.domain.di.modules

import android.app.Application
import android.content.SharedPreferences
import br.com.questa.danone.android.domain.data.datasources.SearchBookDataSource
import br.com.questa.danone.android.domain.data.repositories.UserRepository
import br.com.questa.danone.android.domain.interactors.AuthenticateUserInteractor
import br.com.questa.danone.android.domain.interactors.SearchBookInteractor
import br.com.questa.danone.android.domain.interactors.executors.PostExecutionThread
import br.com.questa.danone.android.domain.interactors.executors.ThreadExecutor
import br.com.questa.danone.android.domain.interactors.executors.impl.BackgroundThreadExecutor
import br.com.questa.danone.android.domain.interactors.executors.impl.MainThreadExecutor
import br.com.questa.danone.android.domain.model.AppSession
import br.com.questa.danone.android.support.UploadFileManager
import br.com.questa.danone.android.ui.presenters.SearchBookPresenter
import br.com.questa.danone.android.ui.presenters.SignInPresenter
import br.com.questa.danone.android.ui.views.AppRouter
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = arrayOf(NetModule::class, DataModule::class, AndroidModule::class))
class ApplicationModule(val application: Application) {
    @Provides fun providesAppSession() : AppSession {
        val session = AppSession.currentSession()
        if (session != null) {
            return session
        }

        return AppSession()
    }
    @Singleton @Provides fun providesAppRouter() : AppRouter = AppRouter()
    @Provides fun providesThreadExecutor() : ThreadExecutor = BackgroundThreadExecutor()
    @Provides fun providesPostThreadExecution() : PostExecutionThread = MainThreadExecutor()
    @Provides fun providesSearchBookInteractor(searchBookDataSource: SearchBookDataSource, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : SearchBookInteractor = SearchBookInteractor(searchBookDataSource, threadExecutor, postExecutionThread)
    @Provides fun providesAuthenticateUserInteractor(userRepository: UserRepository, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : AuthenticateUserInteractor = AuthenticateUserInteractor(userRepository, threadExecutor, postExecutionThread)

    @Provides fun providesSignInPresenter(authenticateUserInteractor: AuthenticateUserInteractor) : SignInPresenter = SignInPresenter(authenticateUserInteractor)
    @Provides fun providesSearchBookPresenter(searchBookInteractor: SearchBookInteractor) : SearchBookPresenter = SearchBookPresenter(searchBookInteractor)
    @Provides fun providesUploadFileManager(gson: Gson, preferences: SharedPreferences) : UploadFileManager = UploadFileManager(gson, preferences)

}