package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class BookModel(
        @Expose var id : String?,
        @Expose @SerializedName("client_id") var cliendId : String?,
        @Expose var files : MutableList<FileModel> = mutableListOf(),
        @Expose @SerializedName("total_files") var filesAmount : Int = 0,
        @Expose var client : CooperatedModel,
        @Expose @SerializedName("status_id") var statusId : Int = 0,
        @Expose @SerializedName("send_files") var canSendFiles : Boolean = false) : Parcelable {

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<BookModel> = object : Parcelable.Creator<BookModel> {
            override fun createFromParcel(source: Parcel): BookModel = BookModel(source)
            override fun newArray(size: Int): Array<BookModel?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(),
                                        source.readString(),
                                        mutableListOf(),
                                        source.readInt(),
                                        source.readParcelable(CooperatedModel::class.java.classLoader),
                                        source.readInt(),
                                        (source.readInt() == 1)) {
        source.readList(files, FileModel::class.java.classLoader)
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(cliendId)
        dest?.writeInt(filesAmount)
        dest?.writeParcelable(client, flags)
        dest?.writeInt(statusId)
        dest?.writeInt(if (canSendFiles) 1 else 0)
        dest?.writeList(files)
    }

    override fun describeContents(): Int = 0
}