package br.com.questa.danone.android.support

import android.content.SharedPreferences
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.domain.enums.UploadState
import br.com.questa.danone.android.domain.model.BookModel
import br.com.questa.danone.android.domain.model.UploadItemModel
import com.google.gson.Gson
import java.util.*
import javax.inject.Inject


class UploadFileManager {
    lateinit var gson : Gson
    lateinit var preferences : SharedPreferences

    private val itemKeyPattern = "coop_%s_index_%d"
    private fun key(uploadItemModel: UploadItemModel) : String = String.format(Locale.getDefault(), itemKeyPattern, uploadItemModel.relatedBookModel.id, uploadItemModel.index)

    @Inject constructor(gson: Gson, preferences : SharedPreferences) {
        this.gson = gson
        this.preferences = preferences
    }

    fun store(uploadItemModel: UploadItemModel) : Boolean = preferences.edit().putString(key(uploadItemModel), gson.toJson(uploadItemModel)).commit()

    fun retriveList(bookModel: BookModel) : List<UploadItemModel> {
        val list : MutableList<UploadItemModel> =  mutableListOf()
        for (x in 1..DanoneApplication.imageUploadLimit) {
            val key = String.format(Locale.getDefault(), itemKeyPattern, bookModel.id, x)
            val raw = preferences.getString(key, null)
            if (raw != null) {
                var uploadItem = gson.fromJson(raw, UploadItemModel::class.java)
                list.plusAssign(uploadItem)
            } else {
                list.plusAssign(UploadItemModel(
                        null,
                        null,
                        UploadState.IDLE,
                        x,
                        bookModel))
            }
        }
        return list
    }
}