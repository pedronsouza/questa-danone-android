package br.com.questa.danone.android.domain.interactors.subscribers

import android.os.Parcelable

open class RunnableSubscriber<in T : Parcelable>(val func : (T) -> Unit) : DefaultSubscriber<T>() {
    override fun onCastedNext(value: T) {
        func(value)
    }
}