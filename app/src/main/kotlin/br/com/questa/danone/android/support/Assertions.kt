package br.com.questa.danone.android.support


class Assertions {
    companion object {
        fun notNull(any : Any?, msg : String? = null) {
            if (any == null)
                throw RuntimeException(msg)
        }
    }
}