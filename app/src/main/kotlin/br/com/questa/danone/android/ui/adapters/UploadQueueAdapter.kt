package br.com.questa.danone.android.ui.adapters

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.enums.UploadState
import br.com.questa.danone.android.domain.model.UploadItemModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso


class UploadQueueAdapter(var items : MutableList<UploadItemModel>, val clicks : (UploadItemModel) -> Unit, val viewId : Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = if (items[position].state == UploadState.SUCCESS) 0 else 1
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as QueueItem?)?.update(items[position], position)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            UploadItemViewHolder(LayoutInflater.from(parent?.context).inflate(viewId, parent, false))
        } else {
            TakePictureViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.view_take_picture_item, parent, false), clicks)
        }
    }
    class TakePictureViewHolder(itemView: View, val clicks : (UploadItemModel) -> Unit) : RecyclerView.ViewHolder(itemView), QueueItem {
        override fun update(uploadItemModel: UploadItemModel, position: Int) {
            itemView.setOnClickListener { clicks(uploadItemModel) }
        }
    }

    class UploadItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), QueueItem {
        val icon by lazy { itemView.findViewById(R.id.upload_image) as ImageView? }
        val progressBar by lazy { itemView.findViewById(R.id.thumb_progress) as ProgressBar? }

        override fun update(uploadItemModel: UploadItemModel, position: Int) {
            if (uploadItemModel.resource != null) {
                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.ARGB_8888
                val bitmap = BitmapFactory.decodeFile(uploadItemModel.resource?.path, options)
                icon?.setImageBitmap(bitmap)
            } else if (uploadItemModel.resourceUrl != null) {
                progressBar?.visibility = View.VISIBLE
                val w = itemView.layoutParams.width
                val h = itemView.layoutParams.height
                Picasso.with(itemView.context).load(uploadItemModel.resourceUrl).resize(w,h).into(icon, object: Callback {
                    override fun onSuccess() = progressBar?.setVisibility(View.GONE)!!
                    override fun onError() = progressBar?.setVisibility(View.GONE)!!
                })
            } else {
                icon?.setImageResource(R.drawable.ic_img_thumb)
            }
        }
    }

    interface QueueItem {
        fun update(uploadItemModel: UploadItemModel, position: Int)
    }
}