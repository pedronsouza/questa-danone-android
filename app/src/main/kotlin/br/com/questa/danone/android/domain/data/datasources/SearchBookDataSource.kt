package br.com.questa.danone.android.domain.data.datasources

import br.com.questa.danone.android.domain.model.SearchBookModel
import rx.Observable


interface SearchBookDataSource {
    fun searchBooksByQuery(query: String?, page : Int = 1, period : String? = null) : Observable<SearchBookModel>
}