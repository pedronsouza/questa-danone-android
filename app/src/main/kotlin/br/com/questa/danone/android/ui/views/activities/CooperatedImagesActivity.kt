package br.com.questa.danone.android.ui.views.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.enums.UploadState
import br.com.questa.danone.android.domain.model.BookModel
import br.com.questa.danone.android.domain.model.FileModel
import br.com.questa.danone.android.domain.model.UploadItemModel
import br.com.questa.danone.android.domain.services.events.OnImageUploadError
import br.com.questa.danone.android.domain.services.events.OnImageUploadSuccess
import br.com.questa.danone.android.ui.adapters.UploadQueueAdapter
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rx.Subscription


class CooperatedImagesActivity : CooperatedDetailActivity() {
    companion object { val bookModelKey = "bookModelKey" }

    private val bookModelImg by lazy { intent?.extras?.getParcelable<BookModel>(bookModelKey) }
    private val recyclerGrid by lazy { findViewById(R.id.images_list) as RecyclerView? }
    private val counter by lazy { findViewById(R.id.upload_count) as TextView? }
    private val content by lazy { findViewById(R.id.root_layout) }

    override fun layoutResourceId(): Int = R.layout.activity_cooperated_images

    var btSendImagesSubs : Subscription? = null
    var btFinalizarSubs : Subscription? = null


    override fun inject(): Unit? {
        DanoneApplication.appComponent?.inject(this)
        return super.inject()
    }

    override fun onBindEvents() {

    }

    override fun unsubscribe() {
        super.unsubscribe()
        btSendImagesSubs?.unsubscribe()
        btFinalizarSubs?.unsubscribe()
    }

    override fun onViewsBind(savedInstanceState: Bundle?) {
        mergeLists(bookModelImg!!)
        counter?.text = "${bookModelImg?.files?.size} de ${maxImagesUpload!!}"
        resultIndex = savedInstanceState?.getInt(resultIndexKey, -1) ?: -1

        val uploadArr = savedInstanceState?.getParcelableArray(uploadInfoKey)
        if (uploadArr != null) {
            uploadItems = uploadArr.toMutableList() as MutableList<UploadItemModel>
        }
        val displaymetrics = resources.displayMetrics
        val screenW = displaymetrics.widthPixels
        val imageWidth = resources.getDimensionPixelSize(R.dimen.image_thumb_size_grid)
        val columns = (screenW / imageWidth)


        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar?.title = "Cooperado ${bookModelFor().id}"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_white)
        }

        recyclerGrid?.layoutManager = GridLayoutManager(this, columns)
        val list = bookModelImg?.files!!.transformInUploadItemsList()

        if (list.size < (maxImagesUpload ?: DanoneApplication.imageUploadLimit)) {
            list.plusAssign(UploadItemModel(
                    null,
                    null,
                    UploadState.IDLE,
                    0,
                    bookModelFor()))
        }

        adapter = UploadQueueAdapter(list, { itemModel ->
            val book = bookModelFor()
            if (!isNetworkAvailable()) {
                showNoNetworkAlert()
            } else if (!book.canSendFiles) {
                showCantSendFiles()
            } else {
                CooperatedDetailActivityPermissionsDispatcher.launchCameraWithCheck(this)
            }

        }, R.layout.view_image_item_thumb)
        recyclerGrid?.adapter = adapter

    }

    fun MutableList<FileModel>.transformInUploadItemsList() : MutableList<UploadItemModel> {
        val out = mutableListOf<UploadItemModel>()
        this.forEach { out.plusAssign(UploadItemModel(
                                    null,
                                    it.imageUrl,
                                    UploadState.SUCCESS,
                                    it.index,
                                    bookModelFor())) }
        return out
    }

    override fun startServicesForSelectedImages() {
        Snackbar.make(content, "Iniciando upload das imagens!", Snackbar.LENGTH_SHORT).show()
        super.startServicesForSelectedImages()
    }

    override fun bookModelFor() : BookModel {
        return bookModelImg!!
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun updateCounter(event: OnImageUploadSuccess) {
        bookModelImg?.files?.plusAssign(FileModel(event.uploadItemModel.resourceUrl, event.uploadItemModel.index, event.uploadItemModel.resourceUrl))

        val intent = Intent()
        intent.putExtra(CooperatedDetailActivity.modelUpdatedKey, bookModelImg)
        setResult(Activity.RESULT_OK, intent)

        val lst = bookModelImg?.files!!.transformInUploadItemsList()

        if (lst.size < (maxImagesUpload ?: DanoneApplication.imageUploadLimit)) {
            lst.plusAssign(UploadItemModel(
                    null,
                    null,
                    UploadState.IDLE,
                    0,
                    bookModelFor()))
            adapter?.items = lst
            adapter?.notifyDataSetChanged()
        }

        counter?.text = "${bookModelImg?.files?.size} de ${maxImagesUpload!!}"
    }

    fun onError(event: OnImageUploadError) {
        Snackbar.make(content, "Ocorreu um erro ao enviar a imagem #${event.uploadItemModel.index + 1}", Snackbar.LENGTH_SHORT).show()
    }
}