package br.com.questa.danone.android.domain.enums

import java.io.Serializable


enum class UploadState : Serializable{
    IDLE,
    START,
    PROGRESS,
    FAILED,
    SUCCESS
}