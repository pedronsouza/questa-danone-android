package br.com.questa.danone.android.ui.views.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.annotation.RequiresPermission
import android.support.design.widget.Snackbar
import android.support.v4.content.FileProvider
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import br.com.concretesolutions.canarinho.formatador.Formatador
import br.com.questa.danone.android.BuildConfig
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.enums.UploadState
import br.com.questa.danone.android.domain.model.BookModel
import br.com.questa.danone.android.domain.model.FileModel
import br.com.questa.danone.android.domain.model.UploadItemModel
import br.com.questa.danone.android.domain.services.ImageUploadService
import br.com.questa.danone.android.domain.services.events.OnImageUploadError
import br.com.questa.danone.android.domain.services.events.OnImageUploadSuccess
import br.com.questa.danone.android.support.UploadFileManager
import br.com.questa.danone.android.ui.adapters.UploadQueueAdapter
import br.com.questa.danone.android.ui.views.activities.bases.ToolbarActivity
import com.crashlytics.android.Crashlytics
import com.jakewharton.rxbinding.view.RxView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import rx.Subscription
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@RuntimePermissions
open class CooperatedDetailActivity : ToolbarActivity() {
    companion object {
        val extraBookModel = "extraBookModel"
        val extraMinImages = "extraMinImages"
        val extraMaxImages = "extraMaxImages"
        val extraNotice = "extraNotice"
        val extraNoticiePhoto = "extraNoticiePhoto"
        val extraBulletInfo = "extraBulletInfo"
        val resultIndexKey = "resultIndexKey"
        val uploadInfoKey = "uploadInfoKey"
        val modelUpdateCode = 101
        val modelUpdatedKey = "modelUpdatedKey"
    }

    val companyName by lazy { findViewById(R.id.company_name) as TextView? }
    val companyCnpj by lazy { findViewById(R.id.company_cnpj) as TextView? }
    val disclaimer  by lazy { findViewById(R.id.disclaimer) as TextView? }
    val photoBulletInfo  by lazy { findViewById(R.id.photo_notice_bullets) as TextView? }
    val imgButton   by lazy { findViewById(R.id.send_imagens_button) }
    val addImgBt    by lazy { findViewById(R.id.take_picture_photo) }
    val seeAllBt    by lazy { findViewById(R.id.see_all_button) }
    val photoNoticeView by lazy { findViewById(R.id.photo_notice) as TextView? }
    val photosCounter by lazy { findViewById(R.id.photos_counter_label) as TextView? }
    val warningState by lazy { findViewById(R.id.upload_warning_state) }
    var warningStateShowed = false
    val rootLayout by lazy { findViewById(R.id.root_layout) }


    val bookModel by lazy { intent?.extras?.getParcelable<BookModel>(extraBookModel) }
    val minImagestoStart by lazy { intent?.extras?.getInt(extraMinImages, DanoneApplication.minImageUploadStart) }
    val maxImagesUpload by lazy { intent?.extras?.getInt(extraMaxImages, DanoneApplication.imageUploadLimit) }
    val htmlNotice by lazy { intent?.extras?.getString(extraNotice, boldSpannable(this, R.string.send_images_msg, R.string.send_images_msg_span, R.color.colorPrimaryDark).toString()) }
    val photoNotice by lazy { intent?.extras?.getString(extraNoticiePhoto, "") }
    val bulletInfo by lazy { intent?.extras?.getCharSequence(extraBulletInfo, "") }

    val loadingState by lazy { findViewById(R.id.start_upload_state) }

//    val recycler    by lazy { findViewById(R.id.upload_queue) as RecyclerView }
    var adapter : UploadQueueAdapter? = null

    val cameraRequestCode = 456
    var uploadItems : MutableList<UploadItemModel> = mutableListOf()

    var addImgBtSubs : Subscription? = null
    var imgBtSubs : Subscription? = null
    var seeAllBtSubs : Subscription? = null
    var proceedBtSubs : Subscription? = null

    var resultIndex = -1

    @Inject
    lateinit var uploadManager : UploadFileManager

    var totalPics = 0

    private var imageUri : Uri? = null

    override fun toolbarResourceId(): Int = R.id.main_toolbar
    override fun textResId(): Int = 0
    override fun layoutResourceId(): Int = R.layout.activity_cooperated_detail

    override fun inject(): Unit? {
        DanoneApplication.appComponent?.inject(this)
        return super.inject()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.sign_out_bt) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.alerta)
                    .setMessage("Você deseja finalizar sua sessão?")
                    .setNegativeButton("Cancelar", null)
                    .setPositiveButton("Sair", { x, v ->
                        appSession.disconnect()
                        appRouter.toSignInScreen(this)
                        finish()
                    }).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBindEvents() {
        super.onBindEvents()
        addImgBtSubs = RxView.clicks(addImgBt).subscribe {
            val book = bookModelFor()
            if (book.canSendFiles) {
                if (!warningStateShowed) {
                    warningStateShowed = true
                    warningState?.visibility = View.VISIBLE
                } else if (totalPics < minImagestoStart!!) {
                    CooperatedDetailActivityPermissionsDispatcher.launchCameraWithCheck(this)
                } else if (totalPics >= minImagestoStart!!) {
                    appRouter.toCooperatedImages(this, bookModelFor(), minImagestoStart!!, maxImagesUpload!!)
                }
            } else {
                showCantSendFiles()
            }
        }

        imgBtSubs = RxView.clicks(imgButton).subscribe {
            if (totalPics >= minImagestoStart!!) {
                appRouter.toCooperatedImages(this, bookModelFor(), minImagestoStart!!, maxImagesUpload!!)
                finish()
            } else {
                showNotAllFilesError()
            }

        }

        seeAllBtSubs = RxView.clicks(seeAllBt).subscribe {
            if(totalPics > 0) {
                appRouter.toCooperatedImages(this, bookModel!!, minImagestoStart!!, maxImagesUpload!!)
            } else {
                Snackbar.make(rootLayout, "Você deve adicionar ao menos uma imagem", Snackbar.LENGTH_SHORT).show()
            }
        }

        proceedBtSubs = RxView.clicks(findViewById(R.id.proceed_button)).subscribe {
            warningState?.visibility = View.GONE
            Crashlytics.log("Proceeding to Camera Launch")
            CooperatedDetailActivityPermissionsDispatcher.launchCameraWithCheck(this)
        }
    }

    override fun unsubscribe() {
        addImgBtSubs?.unsubscribe()
        imgBtSubs?.unsubscribe()
        seeAllBtSubs?.unsubscribe()
        seeAllBtSubs?.unsubscribe()
        proceedBtSubs?.unsubscribe()
        super.unsubscribe()
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onStop() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onViewsBind(savedInstanceState: Bundle?) {
        super.onViewsBind(savedInstanceState)
        mergeLists(bookModel!!, maxImagesUpload!!)
        resultIndex = savedInstanceState?.getInt(resultIndexKey, -1) ?: -1

        val uploadArr = savedInstanceState?.getParcelableArray(uploadInfoKey)
        if (uploadArr != null) {
            uploadItems = uploadArr.toMutableList() as MutableList<UploadItemModel>
        }

        val clientName = if (bookModel?.client?.fantasyName != null && bookModel?.client?.fantasyName!!.length > 0) bookModel?.client?.fantasyName else bookModel?.client?.corporateName

        supportActionBar?.title = "Cooperado ${bookModel?.id}"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_white)

        companyName?.text = clientName
        companyCnpj?.text = Formatador.CNPJ.formata(bookModel?.client?.cnpj)
//        adapter = UploadQueueAdapter(uploadItems, { itemModel ->
//            if (isNetworkAvailable()) {
//                resultIndex = itemModel.index
//                launchCamera()
//            } else {
//                showNoNetworkAlert()
//            }
//
//        }, R.layout.view_upload_item_vertical)
//
//        recycler.adapter = adapter

        totalPics = bookModel?.files?.size ?: 0
        photosCounter?.text = "Você já enviou ${totalPics} de ${maxImagesUpload ?: DanoneApplication.imageUploadLimit} fotos"
        disclaimer?.setText(Html.fromHtml(htmlNotice), TextView.BufferType.SPANNABLE)
        photoNoticeView?.text = photoNotice
        photoBulletInfo?.text = bulletInfo
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(resultIndexKey, resultIndex)
        outState?.putParcelableArray(uploadInfoKey, uploadItems.toTypedArray())
        outState?.putParcelable(CooperatedDetailActivity.extraBookModel, bookModel)
        if (minImagestoStart != null)
            outState?.putInt(CooperatedDetailActivity.extraMinImages, minImagestoStart!!)
        if (maxImagesUpload != null)
            outState?.putInt(CooperatedDetailActivity.extraMaxImages, maxImagesUpload!!)

        outState?.putString(CooperatedDetailActivity.extraNotice, htmlNotice)
        outState?.putString(CooperatedDetailActivity.extraNoticiePhoto, photoNotice)

        super.onSaveInstanceState(outState)
    }

    private var progress : ProgressDialog? = null


    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val selectedBitmap = grabImage()
        if ((resultCode == Activity.RESULT_OK && requestCode == cameraRequestCode && data != null && resultCode != Activity.RESULT_CANCELED) || (resultCode == Activity.RESULT_OK && requestCode == cameraRequestCode && selectedBitmap != null)) {
            if (selectedBitmap != null) {
                if (resultIndex == -1)
                    resultIndex = 0
                else
                    resultIndex++

                val file = tempFilefromBitmap(selectedBitmap)
                val item = UploadItemModel(file,
                        null,
                        UploadState.START,
                        resultIndex,
                        bookModel ?: bookModelFor())
                val service = Intent(this, ImageUploadService::class.java)
                service.putExtra(ImageUploadService.UPLOAD_ITEM_KEY, item)
                startService(service)
                progress = ProgressDialog(this)
                progress?.setMessage("Iniciando Upload da imagem, você deve aguardar até o fim do upload para realizar novas ações")
                progress?.setCanceledOnTouchOutside(false)
                progress?.setCancelable(false)
                progress?.show()
                Snackbar.make(rootLayout, "Iniciando Upload da imagem", Snackbar.LENGTH_SHORT).show()
            } else {
                Crashlytics.log("Bitmap null on grab image")
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == modelUpdateCode) {
            val updatedBookModel = data?.getParcelableExtra<BookModel>(modelUpdatedKey)
            bookModel?.files = updatedBookModel?.files!!
            totalPics = bookModel?.files?.size ?: 0
            photosCounter?.text = "Você já enviou ${totalPics} de ${maxImagesUpload ?: DanoneApplication.imageUploadLimit} fotos"
        } else {
            Crashlytics.log("Result is cancelled or data is null on camera image pick action result")
        }
    }

    open fun bookModelFor() : BookModel {
        return bookModel!!
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        CooperatedDetailActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    fun mergeLists(model: BookModel, subListLimit: Int = -1) {
        uploadItems = UploadItemModel.defaultList(model, maxImagesUpload!!)

        var storedList = uploadManager.retriveList(model)
        storedList = storedList.filter { it.state != UploadState.IDLE }
        if (storedList.size > 0 && storedList.size <= uploadItems.size) {
            for (u in storedList) {
                if (u.index != -1 && u.index < maxImagesUpload!!) {
                    uploadItems[u.index] = u
                }
            }
        }

        if (model.files.size > 0) {
            for (file in model.files) {
                if (file.index != -1 && file.index < maxImagesUpload!!) {
                    uploadItems[file.index] = UploadItemModel(
                            null,
                            file.imageUrl,
                            UploadState.SUCCESS,
                            file.index,
                            model)
                }
            }
        }

        val uploadManager = DanoneApplication.appComponent?.uploadFileManager()!!
        val savedList = uploadManager.retriveList(model)
        val filteredSavedFailed = savedList.filter { it.state == UploadState.FAILED }

        for (file in filteredSavedFailed) {
            if (file.index != -1 && file.index < maxImagesUpload!!) {
                uploadItems[file.index] = file
            }
        }

        if (subListLimit != -1) {
            uploadItems = uploadItems.subList(0, subListLimit)
        }
    }

    fun tempFilefromBitmap(bitmap: Bitmap?) : File {
        val file = File.createTempFile("tempImage_${Date().time}", ".jpg", cacheDir)
        val outputStream = ByteArrayOutputStream()

        val compressFormat = when(file.extension) {
            "jpg" -> Bitmap.CompressFormat.JPEG
            "jpgeg" -> Bitmap.CompressFormat.JPEG
            else -> {
                Bitmap.CompressFormat.PNG
            }
        }

        bitmap?.compress(compressFormat, 50, outputStream)
        val data = outputStream.toByteArray()

        val fos = FileOutputStream(file)
        fos.write(data)
        fos.flush()
        fos.close()
        return file
    }

    private var absolutePath : String? = null
    private var tmpFile : File? = null

    @NeedsPermission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun launchCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            try {
                Crashlytics.log("Camera Launching")
                createImageFile()
                imageUri = FileProvider.getUriForFile(this, "${applicationContext.packageName}.fileprovider", tmpFile)

                if(imageUri != null)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri!!)

                startActivityForResult(intent, cameraRequestCode)
                Crashlytics.log("Camera Launched")
            } catch (e : Throwable) {
                Log.d("lauchCamera", Log.getStackTraceString(e))
                Crashlytics.logException(e)
            }



        } else {
            Crashlytics.logException(Throwable("error on resolver to encounter packageManager"))
        }
    }

    fun createImageFile() {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        absolutePath = image.absolutePath
        tmpFile = image
    }

    fun grabImage() : Bitmap? {

        var bitmap : Bitmap? = null
        try
        {
            contentResolver.notifyChange(imageUri, null)
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
            return bitmap
        }
        catch (e : RuntimeException) {
            Log.d("CameraResult", "Failed to load", e)
            Crashlytics.logException(e)
        }
        catch (e : Throwable)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show()
            Log.d("CameraResult", "Failed to load", e)
            Crashlytics.logException(e)
        }

        return bitmap
    }

    fun showNotAllFilesError() {
        AlertDialog.Builder(this)
                .setTitle(R.string.alerta)
                .setMessage("Para completar o envio, é necessário anexar, no mínimo, ${minImagestoStart!!} imagens")
                .setNegativeButton("OK, Entendi", {  view, which -> view.dismiss() })
                .show()
    }
    fun showCantSendFiles() {
        AlertDialog.Builder(this)
                .setTitle(R.string.alerta)
                .setMessage("Não é possível enviar imagens para esta solictação")
                .setNegativeButton("OK, Entendi", {  view, which -> view.dismiss() })
                .show()
    }

    fun showNoFilesError() {
        AlertDialog.Builder(this)
                .setTitle(R.string.alerta)
                .setMessage("Por favor, adicione ao menos uma imagem")
                .setNegativeButton("OK, Entendi", {  view, which -> view.dismiss() })
                .show()
    }

    @RequiresPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    open fun startServicesForSelectedImages() {
        if (isNetworkAvailable()) {
            loadingState?.visibility = View.VISIBLE
            val filtered = uploadItems.filter { item -> item.state != UploadState.SUCCESS && item.resource != null }
            if(filtered.isEmpty()) {
                showNoFilesError()
            } else {
                for (u in filtered) {
                    if (u.state == UploadState.START) {
                        val service = Intent(this, ImageUploadService::class.java)
                        service.putExtra(ImageUploadService.UPLOAD_ITEM_KEY, u)
                        startService(service)
                    }
                }
            }
        } else {
            showNoNetworkAlert()
        }


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUploadSuccess(event: OnImageUploadSuccess) {
        totalPics++
        progress?.hide()
        Snackbar.make(rootLayout, "Imagem número #${event.uploadItemModel.index + 1} enviada com sucesso", Snackbar.LENGTH_SHORT).show()

        if (this is CooperatedImagesActivity) {

        } else {
            val model = bookModelFor()
            model.files.plusAssign(FileModel(
                    event.uploadItemModel.resourceUrl,
                    event.uploadItemModel.index,
                    event.uploadItemModel.resourceUrl))
        }

        loadingState?.visibility = View.GONE
        photosCounter?.text = "Você já enviou $totalPics de ${maxImagesUpload!!} fotos"
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUploadError(event: OnImageUploadError) {
        progress?.hide()
        if (event.fileModel != null) {
            bookModel?.files?.plusAssign(event.fileModel!!)
        }

        if (this is CooperatedImagesActivity) {

        } else {
            val model = bookModelFor()
            model.files.plusAssign(FileModel(
                    event.uploadItemModel.resourceUrl,
                    event.uploadItemModel.index,
                    event.uploadItemModel.resourceUrl))
            loadingState?.visibility = View.GONE
            appRouter.toCooperatedImages(this, model, minImagestoStart!!, maxImagesUpload!!)
            finish()
        }

    }
}
