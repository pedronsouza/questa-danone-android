package br.com.questa.danone.android.domain.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import br.com.questa.danone.android.DanoneApplication
import dagger.Module
import dagger.Provides

@Module
class AndroidModule(val application: Application) {
    @Provides fun providesSharedPreferences() : SharedPreferences = application.getSharedPreferences(DanoneApplication.sharedPrefsKey, Context.MODE_PRIVATE)
    @Provides fun providesApplicationContext() : Application = application
}