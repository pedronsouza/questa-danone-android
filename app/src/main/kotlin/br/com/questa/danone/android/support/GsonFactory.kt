package br.com.questa.danone.android.support

import br.com.questa.danone.android.domain.net.serializers.LocalDateTimeSerializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.threeten.bp.LocalDateTime

class GsonFactory {
    companion object {
        fun create() : Gson = GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeSerializer())
                .create()
    }
}
