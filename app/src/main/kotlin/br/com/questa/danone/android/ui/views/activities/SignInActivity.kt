package br.com.questa.danone.android.ui.views.activities

import android.os.Bundle
import android.widget.TextView
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.screens.SignInScreen
import br.com.questa.danone.android.ui.presenters.SignInPresenter
import br.com.questa.danone.android.ui.views.activities.bases.BaseActivity
import br.com.questa.danone.android.ui.widgets.LightTextField
import com.jakewharton.rxbinding.view.RxView
import it.sephiroth.android.library.tooltip.Tooltip
import rx.Subscription
import javax.inject.Inject


class SignInActivity : BaseActivity(), SignInScreen {
    private val emailField by lazy { findViewById(R.id.sign_in_login_field) as LightTextField? }
    private val pwdField by lazy { findViewById(R.id.sign_in_pwd_field) as LightTextField? }
    private val signInBt by lazy { findViewById(R.id.sign_in_bt) as TextView? }

    var signInClickSubs : Subscription? = null

    @Inject lateinit var presenter : SignInPresenter

    override fun layoutResourceId(): Int = R.layout.activity_sign_in
    override fun redirectToSearchBookScreen() = appRouter.toSearchBookScreen(this)

    override fun inject() {
        super.inject()
        DanoneApplication.appComponent?.inject(this)
    }

    override fun onViewsBind(savedInstanceState: Bundle?) {

    }

    override fun unsubscribe() {
        super.unsubscribe()
        signInClickSubs?.unsubscribe()
        presenter.unsubscribe()
    }

    override fun onBindEvents() {
        signInClickSubs = RxView.clicks(signInBt!!).subscribe {
            presenter.signInUser(emailField?.textField?.text?.toString(), pwdField?.textField?.text?.toString(), this) }
        pwdField?.setTooltipClickListener { displayHelpTooltip() }
    }

    private var errotTooltip: Tooltip.TooltipView? = null

    override fun displayErrorTooltip() {
        if (errotTooltip == null) {
            errotTooltip = Tooltip.make(this,
                    Tooltip.Builder(102)
                            .anchor(signInBt, Tooltip.Gravity.TOP)
                            .closePolicy(Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 3000)
                            .activateDelay(800).showDelay(300)
                            .text(resources, R.string.login_error)
                            .withStyleId(R.style.AppTheme_Tooltip)
                            .maxWidth(500)
                            .withArrow(true)
                            .withOverlay(false))
        }

        errotTooltip?.show()

        if (!errotTooltip!!.isShown) {
            errotTooltip?.show()
        }
    }

    private var helpTooltip: Tooltip.TooltipView? = null

    override fun displayHelpTooltip() {
        if (helpTooltip == null) {
            helpTooltip = Tooltip.make(this,
                    Tooltip.Builder(101)
                            .anchor(pwdField?.supportIcon, Tooltip.Gravity.TOP)
                            .closePolicy(Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 3000)
                            .activateDelay(800).showDelay(300)
                            .text(resources, R.string.help_tooltip)
                            .withStyleId(R.style.AppTheme_Tooltip)
                            .maxWidth(800)
                            .withArrow(true)
                            .withOverlay(false))
        }
        if (!helpTooltip!!.isShown) {
            helpTooltip?.show()
        }



    }
}