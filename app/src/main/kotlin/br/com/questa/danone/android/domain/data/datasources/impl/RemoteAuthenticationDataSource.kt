package br.com.questa.danone.android.domain.data.datasources.impl

import br.com.questa.danone.android.domain.data.datasources.AuthenticationDataSource
import br.com.questa.danone.android.domain.model.UserModel
import br.com.questa.danone.android.domain.net.HttpServices
import br.com.questa.danone.android.domain.net.bodies.SignInRequestBody
import rx.Observable
import javax.inject.Inject

class RemoteAuthenticationDataSource : AuthenticationDataSource {
    lateinit var httpServices : HttpServices
    @Inject constructor(httpServices: HttpServices) {
        this.httpServices = httpServices;
    }

    override fun signUpUserWithCredentials(credentials: SignInRequestBody): Observable<UserModel> = httpServices
            .authentication
            .singInUserWithCredentials(credentials)
}