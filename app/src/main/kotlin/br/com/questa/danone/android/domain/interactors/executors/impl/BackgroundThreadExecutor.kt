package br.com.questa.danone.android.domain.interactors.executors.impl


import br.com.questa.danone.android.domain.interactors.executors.ThreadExecutor
import javax.inject.Inject

class BackgroundThreadExecutor : ThreadExecutor {
    @Inject constructor()
    override fun execute(task: Runnable?) = Thread(task).start()
}