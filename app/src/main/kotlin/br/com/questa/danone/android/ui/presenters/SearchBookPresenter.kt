package br.com.questa.danone.android.ui.presenters

import br.com.questa.danone.android.domain.interactors.SearchBookInteractor
import br.com.questa.danone.android.domain.interactors.subscribers.RunnableSubscriber
import br.com.questa.danone.android.domain.model.SearchBookModel
import br.com.questa.danone.android.domain.screens.SearchBookScreen
import javax.inject.Inject


class SearchBookPresenter {
    var searchBookInteractor: SearchBookInteractor
    var data : SearchBookModel? = null

    @Inject constructor(searchBookInteractor: SearchBookInteractor) {
        this.searchBookInteractor = searchBookInteractor
    }

    fun fetchBooksForUser(screen: SearchBookScreen) {
        this.searchBookInteractor.query = screen.getQuery()
        this.searchBookInteractor.period = screen.getPeriod()
        this.searchBookInteractor.page = screen.getPage()
        this.searchBookInteractor.execute(SearchSubscriber(screen, {
            response -> data = response
            screen.setupResultsOnScreen(response)
        }))
    }

    fun hasMorePages() = data?.hasMore ?: false

    class SearchSubscriber(val screen: SearchBookScreen, func : (SearchBookModel) -> Unit) : RunnableSubscriber<SearchBookModel>(func) {
        override fun onError(e: Throwable) {
            screen.emptyResult()
            super.onError(e)
        }
    }

    fun unsubscribe() {
        this.searchBookInteractor.unsubscribe()
    }
}