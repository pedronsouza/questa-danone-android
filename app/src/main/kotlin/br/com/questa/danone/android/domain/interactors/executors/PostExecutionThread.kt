package br.com.questa.danone.android.domain.interactors.executors

import rx.Scheduler

interface PostExecutionThread {
    fun scheduler() : Scheduler
}