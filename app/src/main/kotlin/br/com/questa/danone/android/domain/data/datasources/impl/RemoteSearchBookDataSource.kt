package br.com.questa.danone.android.domain.data.datasources.impl

import br.com.questa.danone.android.domain.data.datasources.SearchBookDataSource
import br.com.questa.danone.android.domain.model.SearchBookModel
import br.com.questa.danone.android.domain.net.HttpServices
import rx.Observable
import javax.inject.Inject


class RemoteSearchBookDataSource : SearchBookDataSource {
    var httpServices : HttpServices
    @Inject constructor(httpServices: HttpServices) { this.httpServices = httpServices }
    override fun searchBooksByQuery(query: String?, page : Int, period : String?): Observable<SearchBookModel> = httpServices.cooperated.getCooperatedList(query, page, period)
}