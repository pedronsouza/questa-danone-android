package br.com.questa.danone.android.ui.views

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.text.Html
import br.com.questa.danone.android.domain.model.BookModel
import br.com.questa.danone.android.domain.model.SearchBookModel
import br.com.questa.danone.android.ui.views.activities.*
import br.com.questa.danone.android.ui.views.activities.bases.BaseActivity
import javax.inject.Inject


class AppRouter {
    @Inject constructor()

    fun toSignInScreen(parent: BaseActivity) {
        val intent = Intent(parent, SignInActivity::class.java)
        parent.startActivity(intent)
    }

    fun toSignInScreenWithClear(parent: Context) {
        val intent = Intent(parent, SignInActivity::class.java)
        parent.startActivity(intent)
    }

    fun toSendImagesButtonScreen(parent: BaseActivity) {
        val intent = Intent(parent, MainActivity::class.java)
        parent.startActivity(intent)
    }

    fun toSearchBookScreen(parent: BaseActivity) {
        val intent = Intent(parent, SearchBookActivity::class.java)
        parent.startActivity(intent)
    }

    fun toCooperatedDetail(parent: Context, bookModel: BookModel, searchBookModel: SearchBookModel) {
        val intent = Intent(parent, CooperatedDetailActivity::class.java)
        intent.putExtra(CooperatedDetailActivity.extraBookModel, bookModel)
        intent.putExtra(CooperatedDetailActivity.extraMinImages, searchBookModel.photos.size)
        intent.putExtra(CooperatedDetailActivity.extraMaxImages, searchBookModel.maxItems)
        intent.putExtra(CooperatedDetailActivity.extraNotice, searchBookModel.notice)
        intent.putExtra(CooperatedDetailActivity.extraNoticiePhoto, searchBookModel.photoNotice)
        val out = StringBuilder()

        for (x in 0..(searchBookModel.photos.size - 1)) {
            out.append("&#8226; ${searchBookModel.photos[x]}<br />")
        }

        intent.putExtra(CooperatedDetailActivity.extraBulletInfo, Html.fromHtml(out.toString()))

        parent.startActivity(intent)
    }

    fun toCooperatedImages(parent: AppCompatActivity, bookModel: BookModel, minImages : Int, maxImages : Int) {
        val intent = Intent(parent, CooperatedImagesActivity::class.java)
        intent.putExtra(CooperatedImagesActivity.bookModelKey, bookModel)
        intent.putExtra(CooperatedDetailActivity.extraMinImages, minImages)
        intent.putExtra(CooperatedDetailActivity.extraMaxImages, maxImages)
        parent.startActivityForResult(intent, CooperatedDetailActivity.modelUpdateCode)
    }
}