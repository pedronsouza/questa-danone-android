package br.com.questa.danone.android.domain.net.bodies

import com.google.gson.annotations.Expose

data class SignInRequestBody(@Expose var email : String? = null, @Expose var password : String?)