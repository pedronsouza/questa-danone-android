package br.com.questa.danone.android.domain.screens

import br.com.questa.danone.android.domain.model.PeriodModel
import br.com.questa.danone.android.domain.model.SearchBookModel
import br.com.questa.danone.android.ui.widgets.EndlessRecyclerView


interface SearchBookScreen : EndlessRecyclerView.Listener {
    fun searchBooksWithQuery(query: String)
    fun setupResultsOnScreen(searchBookModel: SearchBookModel)
    fun withResults()
    fun emptyResult()
    fun getPage() : Int
    fun getQuery() : String?
    fun getPeriod() : String?
    fun showFilterDialog()
    fun filterListWithResult(period : PeriodModel?)
    fun showCantSendFilesDialog()
}