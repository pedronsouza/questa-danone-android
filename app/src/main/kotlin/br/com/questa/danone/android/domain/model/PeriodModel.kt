package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose


data class PeriodModel(@Expose var id : String?, @Expose var name: String?) : Parcelable {
    constructor(source: Parcel) : this(source.readString(), source.readString())
    override fun describeContents(): Int = 0
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(name)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<PeriodModel> = object : Parcelable.Creator<PeriodModel> {
            override fun createFromParcel(source: Parcel): PeriodModel = PeriodModel(source)
            override fun newArray(size: Int): Array<PeriodModel?> = arrayOfNulls(size)
        }
    }
}