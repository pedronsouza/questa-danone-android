package br.com.questa.danone.android.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.concretesolutions.canarinho.formatador.Formatador
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.model.BookModel
import br.com.questa.danone.android.domain.model.SearchBookModel


class SearchBookAdapter(var items : Array<BookModel>?, val clicks: (BookModel) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var maxItems = DanoneApplication.imageUploadLimit
    var isLoading : Boolean = false

    override fun getItemViewType(position: Int): Int = if (position == items?.size && isLoading) 1 else  0
    override fun getItemCount(): Int = if (isLoading) ((items?.size ?: 0) + 1) else items?.size ?: 0
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 1) {
            return ProgressViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.view_bottom_loading, parent, false))
        } else {
            return SearchItemViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.view_search_item, parent, false), maxItems, clicks)
        }
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (position < items?.size!!) {
            (holder as SearchItemViewHolder?)?.update(items?.get(position))
        }
    }

    class SearchItemViewHolder(itemView: View, val maxItems : Int, val clicks: (BookModel) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private val labelId by lazy { itemView.findViewById(R.id.search_id) as TextView? }
        private val labelClient by lazy { itemView.findViewById(R.id.search_clientName) as TextView? }
        private val labelDescrp by lazy { itemView.findViewById(R.id.search_description) as TextView? }
        private val counter by lazy { itemView.findViewById(R.id.upload_count) as TextView? }
        private val iconHolder by lazy { itemView.findViewById(R.id.icon_holder) }
        val iconDecor by lazy { itemView.findViewById(R.id.icon_decor) as ImageView? }

        fun update(model: BookModel?) {
            labelId?.text = model?.id
            labelClient?.text = model?.client?.flagName
            val clientName = if (model?.client?.fantasyName != null && model?.client?.fantasyName!!.length > 0) model?.client?.fantasyName else model?.client?.corporateName
            labelDescrp?.text = "$clientName - ${Formatador.CNPJ.formata(model?.client?.cnpj)}"
            itemView.setOnClickListener { clicks(model!!) }
            iconHolder.setOnClickListener { clicks(model!!) }
            counter?.text = "${model?.files?.size}/${maxItems}"

            if (model?.statusId == SearchBookModel.SearchBookStatusApproved || model?.statusId == SearchBookModel.SearchBookStatusApprovedDouble) {
                iconDecor?.setImageResource(R.drawable.ic_thumb_img_decor_success)
                iconDecor?.visibility = View.VISIBLE
            } else if (model?.statusId == SearchBookModel.SearchBookStatusReject) {
                iconDecor?.setImageResource(R.drawable.ic_thumb_img_decor_error)
                iconDecor?.visibility = View.VISIBLE
            } else if (model?.statusId == SearchBookModel.SearchBookStatusOnServer) {
                iconDecor?.setImageResource(R.drawable.ic_thumb_img_decor_start)
                iconDecor?.visibility = View.VISIBLE
            } else {
                iconDecor?.visibility = View.GONE
            }
        }
    }

    class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}