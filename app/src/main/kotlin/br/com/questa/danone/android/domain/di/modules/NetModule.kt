package br.com.questa.danone.android.domain.di.modules

import android.app.Application
import br.com.questa.danone.android.BuildConfig
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.domain.model.AppSession
import br.com.questa.danone.android.domain.net.HttpServices
import br.com.questa.danone.android.support.EnumRetrofitConverterFactory
import br.com.questa.danone.android.support.GsonFactory
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetModule(val application: Application) {
    @Provides @Singleton fun providesHttpClient() : OkHttpClient {
        val logging = HttpLoggingInterceptor()
        if(BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }




        val httpClient = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor({
                    val appSession = AppSession.currentSession()

                    val request = it.request()
                            .newBuilder()
                            .addHeader("User-Agent", "Android")

                    if (appSession != null && appSession.connected())
                        request.addHeader("Authorization", "Bearer ${appSession.user?.accessToken}")


                    it.proceed(request.build())
                })
                .addInterceptor(logging)
                .build()

        return httpClient
    }

    @Provides @Singleton fun providesRetrofit(httpClient: OkHttpClient, gson : Gson) : Retrofit {
        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(EnumRetrofitConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
        return retrofit
    }

    @Provides @Singleton fun providesGson() : Gson = GsonFactory.create()

    @Provides @Singleton fun providesHttpServices(retrofit: Retrofit) : HttpServices = HttpServices(retrofit)
}