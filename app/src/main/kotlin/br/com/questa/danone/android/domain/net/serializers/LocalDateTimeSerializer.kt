package br.com.questa.danone.android.domain.net.serializers


import br.com.questa.danone.android.support.DateUtils
import com.google.gson.*
import org.threeten.bp.LocalDateTime
import java.lang.reflect.Type

class LocalDateTimeSerializer : JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {
    override fun serialize(src: LocalDateTime?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(src?.format(DateUtils.formatter))
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): LocalDateTime? {
        return LocalDateTime.parse(json?.asString!!, DateUtils.formatter)
    }
}