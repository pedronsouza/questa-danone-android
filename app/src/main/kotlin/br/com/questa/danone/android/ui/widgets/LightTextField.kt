package br.com.questa.danone.android.ui.widgets

import android.annotation.TargetApi
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import br.com.questa.danone.android.R
import com.jakewharton.rxbinding.view.RxView
import rx.Subscription


class LightTextField : LinearLayout {
    val textField by lazy { findViewById(R.id.textfield) as EditText? }
    val supportIcon by lazy { findViewById(R.id.supportIcon) as ImageView? }
    private val label by lazy { findViewById(R.id.label) as TextView? }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) { startLayout(attrs, defStyleAttr, defStyleRes) }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { startLayout(attrs, defStyleAttr) }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) { startLayout(attrs) }
    constructor(context: Context) : super(context) { startLayout() }

    private var clickSubs : Subscription? = null

    fun startLayout(attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0) {
        LayoutInflater.from(context).inflate(R.layout.view_textfield_light, this, true)
        if (attrs != null) {
            val typedArr = context.theme.obtainStyledAttributes(attrs, R.styleable.LightTextField, defStyleAttr, defStyleRes)

            try {
                label?.text = typedArr.getString(R.styleable.LightTextField_textForLabel)
                defineImeType(typedArr.getInt(R.styleable.LightTextField_imeType, -1))
                defineInputMode(typedArr.getInt(R.styleable.LightTextField_inputMode, -1))
                defineSupportIcon(typedArr.getDrawable(R.styleable.LightTextField_supportIcon))
                defineLabelVisibility(typedArr.getBoolean(R.styleable.LightTextField_hideLabel, true))
                defineTextFieldHint(typedArr.getString(R.styleable.LightTextField_hintForField))
            } finally {
                typedArr.recycle()
            }
        }

        textField?.isClickable = isClickable
        textField?.isFocusable = isFocusable
    }

    override fun onDetachedFromWindow() {
        clickSubs?.unsubscribe()
        super.onDetachedFromWindow()
    }

    private fun defineLabelVisibility(visible: Boolean) {
        label?.visibility = if (visible) View.VISIBLE else View.GONE
    }

    private fun defineTextFieldHint(hint: String?) {
        if (hint != null) {
            textField?.hint = hint
        }
    }

    private fun defineImeType(imeType : Int) {
        if (imeType != -1) {
            when(imeType) {
                0 -> textField?.imeOptions = EditorInfo.IME_ACTION_NEXT
                else -> textField?.imeOptions = EditorInfo.IME_ACTION_DONE
            }
        } else {
            textField?.imeOptions = EditorInfo.IME_ACTION_NEXT
        }
    }

    private fun defineInputMode(inputMode: Int) {
        if (inputMode != -1) {
            when(inputMode) {
                0 -> textField?.inputType = InputType.TYPE_CLASS_TEXT
                else -> {
                    textField?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    textField?.transformationMethod = PasswordTransformationMethod.getInstance()
                }
            }
        } else {
            textField?.inputType = InputType.TYPE_CLASS_TEXT
        }
    }

    private fun defineSupportIcon(drawable: Drawable?) {
        if (drawable != null) {
            supportIcon?.setImageDrawable(drawable)
        }
    }

    fun setTooltipClickListener(click: () -> Unit) {
        if (supportIcon != null)
            clickSubs = RxView.clicks(supportIcon!!).subscribe { click() }
    }
}