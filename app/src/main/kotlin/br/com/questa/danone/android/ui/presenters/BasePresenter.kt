package br.com.questa.danone.android.ui.presenters


abstract class BasePresenter {
    abstract fun unsubscribe()
}