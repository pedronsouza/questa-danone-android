package br.com.catalyst.domain.di.scopes

import javax.inject.Scope

@Scope
@Retention
annotation class PresenterScope