package br.com.questa.danone.android.domain.net

import br.com.questa.danone.android.domain.net.endpoints.AuthenticationEndpoint
import br.com.questa.danone.android.domain.net.endpoints.CooperatedEndpoint
import retrofit2.Retrofit
import javax.inject.Inject


class HttpServices {
    val authentication by lazy { retrofit.create(AuthenticationEndpoint::class.java) }
    val cooperated by lazy { retrofit.create(CooperatedEndpoint::class.java) }

    var retrofit : Retrofit
    @Inject constructor(retrofit: Retrofit) {
        this.retrofit = retrofit
    }



}