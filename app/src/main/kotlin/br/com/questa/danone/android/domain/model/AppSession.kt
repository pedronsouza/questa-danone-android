package br.com.questa.danone.android.domain.model

import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.support.GsonFactory
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import javax.inject.Inject


class AppSession {
    @Expose var user : UserModel? = null
    fun connected(): Boolean = this.user != null

    companion object {
        @JvmStatic val sessionKey = "sessionKey"
        @JvmStatic fun create(userModel: UserModel) : AppSession {
            val sharedPrefs = DanoneApplication.appComponent?.preferences()
            val newSession = AppSession(userModel)
            sharedPrefs?.edit()?.putString(sessionKey, newSession.toJson())?.commit()
            return newSession
        }

        @JvmStatic fun currentSession () : AppSession? {
            val sessionRaw = DanoneApplication.appComponent?.preferences()?.getString(sessionKey, null)
            if (sessionRaw != null) {
                return GsonFactory.create().fromJson(sessionRaw, AppSession::class.java)
            }

            return null
        }

    }

    @Inject constructor() {

    }

    constructor(userModel: UserModel) {
        this.user = userModel
    }

    fun toJson() : String = GsonBuilder().create().toJson(this)

    fun disconnect() {
        DanoneApplication.appComponent?.preferences()?.edit()?.putString(sessionKey, null)?.commit()
        user = null
    }
}