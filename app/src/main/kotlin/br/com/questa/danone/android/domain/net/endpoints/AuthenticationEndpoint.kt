package br.com.questa.danone.android.domain.net.endpoints

import br.com.questa.danone.android.domain.model.UserModel
import br.com.questa.danone.android.domain.net.bodies.SignInRequestBody
import retrofit2.http.Body
import retrofit2.http.POST
import rx.Observable


interface AuthenticationEndpoint {
    @POST("login")
    fun singInUserWithCredentials(@Body signInRequestBody: SignInRequestBody) : Observable<UserModel>
}