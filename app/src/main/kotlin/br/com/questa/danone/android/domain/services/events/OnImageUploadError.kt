package br.com.questa.danone.android.domain.services.events

import br.com.questa.danone.android.domain.model.FileModel
import br.com.questa.danone.android.domain.model.UploadItemModel


data class OnImageUploadError(var uploadItemModel: UploadItemModel, var fileModel : FileModel? = null)