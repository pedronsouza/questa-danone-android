package br.com.questa.danone.android.ui.views.activities

import android.os.Bundle
import br.com.questa.danone.android.R
import br.com.questa.danone.android.ui.views.activities.bases.BaseActivity


class ImageViewerActivity : BaseActivity() {
    companion object { val urlKey = "urlKey" }
    private val url by lazy { intent?.extras?.getString(urlKey)!! }

    override fun onViewsBind(savedInstanceState: Bundle?) {

    }

    override fun layoutResourceId(): Int = R.layout.activity_image_viewer
}