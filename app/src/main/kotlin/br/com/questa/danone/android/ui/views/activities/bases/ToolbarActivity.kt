package br.com.questa.danone.android.ui.views.activities.bases

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.MenuItem


abstract class ToolbarActivity : BaseActivity() {
    abstract fun toolbarResourceId() : Int
    abstract fun textResId() : Int

    val toolbar by lazy { findViewById(toolbarResourceId()) as Toolbar? }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onViewsBind(savedInstanceState: Bundle?) {
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(textResId())
    }
}