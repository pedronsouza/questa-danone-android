package br.com.questa.danone.android.domain.screens


interface SendImagesButtonScreen {
    fun redirectToLogin()
    fun redirectToBookSearchScreen()
}