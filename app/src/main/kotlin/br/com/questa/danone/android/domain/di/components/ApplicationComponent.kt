package br.com.questa.danone.android.domain.di.components

import android.app.Application
import android.content.SharedPreferences
import br.com.questa.danone.android.domain.di.modules.ApplicationModule
import br.com.questa.danone.android.domain.model.AppSession
import br.com.questa.danone.android.domain.net.HttpServices
import br.com.questa.danone.android.support.UploadFileManager
import br.com.questa.danone.android.ui.views.AppRouter
import br.com.questa.danone.android.ui.views.activities.*
import br.com.questa.danone.android.ui.views.activities.bases.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun inject(activity: BaseActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(mainActivity: SearchBookActivity)
    fun inject(signInActivity: SignInActivity)
    fun inject(signInActivity: CooperatedDetailActivity)
    fun inject(signInActivity: CooperatedImagesActivity)

    fun preferences() : SharedPreferences
    fun httpServices() : HttpServices
    fun uploadFileManager() : UploadFileManager
    fun application() : Application
    fun router() : AppRouter
    fun appSession() : AppSession
}