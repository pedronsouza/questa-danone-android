package br.com.questa.danone.android.ui.views.activities

import android.os.Bundle
import android.widget.ImageView
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.screens.SplashScreen
import br.com.questa.danone.android.ui.views.activities.bases.BaseActivity

class SplashActivity : BaseActivity(), SplashScreen {
    private val logoImage by lazy { findViewById(R.id.logo_imageview) as ImageView? }

    override fun layoutResourceId(): Int = R.layout.activity_splash
    override fun redirectToSendImagesScreen() = appRouter.toSendImagesButtonScreen(this)
    override fun onViewsBind(savedInstanceState: Bundle?) = redirectToSendImagesScreen()
}