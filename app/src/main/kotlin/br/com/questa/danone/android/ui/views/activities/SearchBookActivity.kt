package br.com.questa.danone.android.ui.views.activities


import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.R
import br.com.questa.danone.android.domain.model.PeriodModel
import br.com.questa.danone.android.domain.model.SearchBookModel
import br.com.questa.danone.android.domain.screens.SearchBookScreen
import br.com.questa.danone.android.ui.adapters.SearchBookAdapter
import br.com.questa.danone.android.ui.presenters.SearchBookPresenter
import br.com.questa.danone.android.ui.views.activities.bases.ToolbarActivity
import br.com.questa.danone.android.ui.widgets.EndlessRecyclerView
import br.com.questa.danone.android.ui.widgets.LightTextField
import com.jakewharton.rxbinding.view.RxView
import com.jakewharton.rxbinding.widget.RxTextView
import rx.Subscription
import java.util.concurrent.Executors
import java.util.concurrent.Future
import javax.inject.Inject


class SearchBookActivity : ToolbarActivity(), SearchBookScreen {
    private val recycler by lazy { findViewById(R.id.recycler) as EndlessRecyclerView? }
    private val lightEditText by lazy { findViewById(R.id.edittext) as LightTextField? }
    private val content by lazy { findViewById(R.id.content) }
    private val empty by lazy  { findViewById(R.id.empty_state)}
    private val loadView by lazy { findViewById(R.id.load_view) }
    private val filterLabel by lazy { findViewById(R.id.filter_text) as LightTextField? }

    @Inject lateinit var presenter: SearchBookPresenter

    private var editTextChangeSubs : Subscription? = null
    private var adapter : SearchBookAdapter? = null
    var searchQuery : String? = null

    private var page : Int =  1
    private var selectedPeriod : String? = null

    var threadPoolExecutor = Executors.newSingleThreadExecutor()
    var searchFuture : Future<*>? = null
    var periods : List<PeriodModel>? = null
    var isFirstLoad : Boolean = true

    override fun toolbarResourceId(): Int = R.id.main_toolbar
    override fun layoutResourceId(): Int = R.layout.activity_search_book
    override fun textResId(): Int = R.string.search_book_title
    override fun getPeriod(): String? = selectedPeriod
    override fun getPage() : Int = page
    override fun getQuery(): String? = searchQuery

    override fun inject() {
        super.inject()
        DanoneApplication.appComponent?.inject(this)
    }

    var filterClickSubs : Subscription? = null
    var filterTextClickSubs : Subscription? = null

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.sign_out_bt) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.alerta)
                    .setMessage("Você deseja finalizar sua sessão?")
                    .setNegativeButton("Cancelar", null)
                    .setPositiveButton("Sair", { x, v ->
                        appSession.disconnect()
                        appRouter.toSignInScreen(this)
                        finish()
                    }).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBindEvents() {
        super.onBindEvents()
        recycler?.listener = this
        editTextChangeSubs = RxTextView.textChanges(lightEditText?.textField!!).subscribe { query ->
            if (query?.toString() != searchQuery) {
                page = 1
                if ((query?.toString()?.length ?: 0) == 0 && requestState != BackgroundExecutionState.LOADING) {
                    searchFuture?.cancel(true)
                    searchFuture = null
                    searchQuery = ""
                    page = 1
                    loadView?.visibility = View.VISIBLE
                    requestState = BackgroundExecutionState.LOADING
                    presenter.fetchBooksForUser(this@SearchBookActivity)
                    return@subscribe
                }
            }

            if (searchFuture != null) {
                searchFuture?.cancel(true)
                requestState = BackgroundExecutionState.IDLE
                searchFuture = null
            }

            searchQuery = query?.toString()
            onlyIfIdle(requestState, {
                searchFuture = threadPoolExecutor.submit {
                    if (searchQuery != null && searchQuery!!.isNotEmpty()) {
                        searchBooksWithQuery(searchQuery!!)
                    }
                }
            })

        }
        filterClickSubs = RxView.clicks(findViewById(R.id.filter)).subscribe { showFilterDialog() }
        filterTextClickSubs = RxView.clicks(findViewById(R.id.filter_text)).subscribe { showFilterDialog() }
    }

    override fun unsubscribe() {
        filterClickSubs?.unsubscribe()
        editTextChangeSubs?.unsubscribe()
        filterTextClickSubs?.unsubscribe()
        presenter.unsubscribe()
        super.unsubscribe()
    }

    override fun onResume() {
        super.onResume()
        loadView?.visibility = View.GONE
        if(searchQuery == null || searchQuery!!.isEmpty()) {
            searchBooksWithQuery("")
        }
    }

    override fun onStop() {
        requestState = BackgroundExecutionState.IDLE
        loadView?.visibility = View.GONE
        super.onStop()
    }

    override fun onViewsBind(savedInstanceState: Bundle?) {
        super.onViewsBind(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_white)
    }

    override fun searchBooksWithQuery(query: String) {
        requestState = BackgroundExecutionState.IDLE
        loadView?.visibility = View.VISIBLE
        presenter.fetchBooksForUser(this)
    }

    override fun setupResultsOnScreen(searchBookModel: SearchBookModel) {
        requestState = BackgroundExecutionState.IDLE
        adapter?.isLoading = false
        adapter?.notifyDataSetChanged()

        if (searchBookModel.data.isNotEmpty()) {
            periods = searchBookModel.periods
            if (adapter != null && page != 1) {
                adapter?.items = adapter?.items?.plus(searchBookModel.data)
                adapter?.notifyDataSetChanged()
            } else {
                adapter = SearchBookAdapter(searchBookModel.data, { model ->
                    appRouter.toCooperatedDetail(this, model, searchBookModel)
                })
                adapter?.maxItems = searchBookModel.maxItems
                recycler?.adapter = adapter
            }

            withResults()
        } else if (page == 1) {
            emptyResult()
        } else {
            withResults()
        }

        if (loadView?.visibility != View.GONE) {
            loadView?.visibility = View.GONE
        }
    }

    override fun emptyResult() {
        content.visibility = View.GONE
        loadView.visibility = View.GONE
        empty.visibility = View.VISIBLE
    }

    override fun withResults() {
        content.visibility = View.VISIBLE
        loadView.visibility = View.GONE
        empty.visibility = View.GONE
    }


    override fun loadMore() {
        if (presenter.hasMorePages() && requestState == BackgroundExecutionState.IDLE) {
            requestState = BackgroundExecutionState.LOADING
            page++
            adapter?.isLoading = true
            adapter?.notifyDataSetChanged()
            recycler?.postDelayed({
                presenter.fetchBooksForUser(this)
            }, 300)

        }
    }

    override fun showCantSendFilesDialog() {
        AlertDialog.Builder(this)
                .setTitle(R.string.alerta)
                .setMessage("Não é possível enviar imagens para esta solictação")
                .setNegativeButton("OK, Entendi", {  view, which -> view.dismiss() })
                .show()
    }

    override fun showFilterDialog() {
        if (periods != null) {
            val options : MutableList<String> = mutableListOf()
            periods?.forEach { options.plusAssign(it.name!!) }

            val builder = AlertDialog.Builder(this)
                    .setTitle(R.string.filtrar_por_quinzena)
                    .setItems(options.toTypedArray(), { view, which ->
                        filterListWithResult(periods?.get(which))
                    })

            if (selectedPeriod != null)
                builder.setNegativeButton("Remover Filtro", {view, which ->
                    selectedPeriod = null
                    page = 1
                    loadView.visibility = View.VISIBLE
                    filterLabel?.textField?.text = null
                    presenter.fetchBooksForUser(this@SearchBookActivity)
                })

            builder.show()
        }

    }

    override fun filterListWithResult(period : PeriodModel?) {
        page = 1
        loadView.visibility = View.VISIBLE
        selectedPeriod = period?.id
        filterLabel?.textField?.setText(period?.name)
        presenter.fetchBooksForUser(this)
    }
}