package br.com.questa.danone.android.domain.net.endpoints

import br.com.questa.danone.android.domain.model.SearchBookModel
import br.com.questa.danone.android.domain.net.bodies.DatResponse
import br.com.questa.danone.android.domain.net.bodies.UploadResponseBody
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable


interface CooperatedEndpoint {
    @GET("cooperated")
    fun getCooperatedList(@Query("query") query : String? = null, @Query("page") page : Int = 1, @Query("period") period : String? = null) : Observable<SearchBookModel>

    @POST("cooperated/insert")
    @Multipart
    fun uploadImage(@Part filePart : MultipartBody.Part, @Part("request_cooperated_id") requestCooperatedId : RequestBody) : Observable<DatResponse<UploadResponseBody>>
}