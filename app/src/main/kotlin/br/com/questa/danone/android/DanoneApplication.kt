package br.com.questa.danone.android

import android.app.Application
import br.com.questa.danone.android.domain.di.components.ApplicationComponent
import br.com.questa.danone.android.domain.di.components.DaggerApplicationComponent
import br.com.questa.danone.android.domain.di.modules.AndroidModule
import br.com.questa.danone.android.domain.di.modules.ApplicationModule
import br.com.questa.danone.android.domain.di.modules.DataModule
import br.com.questa.danone.android.domain.di.modules.NetModule
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric


class DanoneApplication : Application() {
    companion object {
        val sharedPrefsKey = "sharedPrefsKey"
        val imageUploadLimit = 20
        val minImageUploadStart = 3
        @JvmStatic var appComponent : ApplicationComponent? = null
    }
    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        appComponent = DaggerApplicationComponent.builder()
                .androidModule(AndroidModule(this))
                .dataModule(DataModule(this))
                .netModule(NetModule(this))
                .applicationModule(ApplicationModule(this))
                .build()

    }
}