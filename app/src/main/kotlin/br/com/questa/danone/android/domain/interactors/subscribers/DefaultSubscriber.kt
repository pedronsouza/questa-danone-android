package br.com.questa.danone.android.domain.interactors.subscribers
import android.os.Parcelable
import android.util.Log
import rx.Subscriber


@Suppress("UNCHECKED_CAST")
abstract class DefaultSubscriber<in T : Parcelable> : Subscriber<Any>() {
    override fun onCompleted() {
        Log.d("DefaultSubscriber", "Subscriber Completed")
    }

    override fun onError(e: Throwable) {
        Log.e("DefaultSubscriber", Log.getStackTraceString(e))
    }

    override fun onNext(t: Any) {
        Log.d("DefaultSubscriber", "Subscriber onNext")
        onCastedNext(t as T)
    }

    abstract fun onCastedNext(value : T)
}