package br.com.questa.danone.android.domain.model

import android.os.Parcel
import android.os.Parcelable
import br.com.questa.danone.android.domain.enums.UploadState
import java.io.File


data class UploadItemModel(
        var resource: File?,
        var resourceUrl : String?,
        var state: UploadState,
        var index : Int,
        var relatedBookModel: BookModel) : Parcelable {
    constructor(source: Parcel) : this(source.readSerializable() as File?,
                                        source.readString(),
                                        source.readSerializable() as UploadState,
                                        source.readInt(),
                                        source.readParcelable(CooperatedModel::class.java.classLoader))
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<UploadItemModel> = object : Parcelable.Creator<UploadItemModel> {
            override fun createFromParcel(source: Parcel): UploadItemModel = UploadItemModel(source)
            override fun newArray(size: Int): Array<UploadItemModel?> = arrayOfNulls(size)
        }

        @JvmStatic fun defaultList(bookModel: BookModel, limit: Int) : MutableList<UploadItemModel> {
            val out : MutableList<UploadItemModel> = mutableListOf()
            for (x in 1..limit) {
                out.plusAssign(UploadItemModel(
                                null,
                                null,
                                UploadState.IDLE,
                        (x - 1),
                        bookModel))
            }

            return out
        }

        @JvmStatic fun replaceNextIdle(list: MutableList<UploadItemModel>, newModel: UploadItemModel, limit: Int) : MutableList<UploadItemModel> {
            for (x in 1..limit) {
                if (list[(x - 1)].state == UploadState.IDLE) {
                    list[(x - 1)] = newModel
                    break
                }
            }

            return list
        }
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeSerializable(resource)
        dest?.writeString(resourceUrl)
        dest?.writeSerializable(state)
        dest?.writeInt(index)
        dest?.writeParcelable(relatedBookModel, flags)
    }

    override fun describeContents(): Int = 0

}