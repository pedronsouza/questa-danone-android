package br.com.questa.danone.android.domain.net.bodies

import android.os.Parcelable
import com.google.gson.annotations.Expose


data class DatResponse<T : Parcelable>(
        @Expose var data : T)
