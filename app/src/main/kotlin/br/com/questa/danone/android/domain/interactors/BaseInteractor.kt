package br.com.questa.danone.android.domain.interactors

import android.os.Parcelable
import android.util.Log
import br.com.questa.danone.android.DanoneApplication
import br.com.questa.danone.android.domain.interactors.executors.PostExecutionThread
import br.com.questa.danone.android.domain.interactors.executors.ThreadExecutor
import br.com.questa.danone.android.domain.interactors.subscribers.DefaultSubscriber
import com.crashlytics.android.Crashlytics
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import rx.Subscription
import rx.schedulers.Schedulers

abstract class BaseInteractor<T : Parcelable>(val threadExecutor : ThreadExecutor, val postExecutionThread : PostExecutionThread) {
    var subscription : Subscription? = null
    abstract protected fun observableFor() : Observable<T>
    fun execute(subscriber : DefaultSubscriber<T>) {
        subscription = observableFor()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler())
                .doOnError { ex ->                                                                                              
                    Log.e("doOnError", Log.getStackTraceString(ex))
                    Crashlytics.logException(ex)
                    if (ex is HttpException) {
                        if (ex.code() == 401) {
                            val appRouter = DanoneApplication.appComponent?.router()
                            val ctx = DanoneApplication.appComponent?.application()
                            val appSession = DanoneApplication.appComponent?.appSession()

                            if (ctx != null) {
                                appSession?.disconnect()
                                appRouter?.toSignInScreenWithClear(ctx)
                            }
                        }

                    }

                }.subscribe(subscriber)
    }

    fun unsubscribe() {
        subscription?.unsubscribe()
    }
}