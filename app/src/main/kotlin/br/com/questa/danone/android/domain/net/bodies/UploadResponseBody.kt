package br.com.questa.danone.android.domain.net.bodies

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by pedronsouza on 24/10/16.
 */
data class UploadResponseBody(
        @Expose @SerializedName("message_error") var error : String?,
        @Expose @SerializedName("upload_status") var stored : Boolean = false,
        @Expose @SerializedName("image_url")     var url : String?) : Parcelable {
    constructor(source: Parcel) : this(source.readString(), (source.readInt() == 1), source.readString()) {

    }
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<UploadResponseBody> = object : Parcelable.Creator<UploadResponseBody> {
            override fun createFromParcel(source: Parcel): UploadResponseBody = UploadResponseBody(source)
            override fun newArray(size: Int): Array<UploadResponseBody?> = arrayOfNulls(size)
        }
    }
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(error)
        dest?.writeInt(if (stored) 1 else 0)
        dest?.writeString(url)
    }

    override fun describeContents(): Int = 0
}