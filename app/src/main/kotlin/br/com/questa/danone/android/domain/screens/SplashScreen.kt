package br.com.questa.danone.android.domain.screens


interface SplashScreen {
    fun redirectToSendImagesScreen()
}